﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ETourGuide.Startup))]
namespace ETourGuide
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
