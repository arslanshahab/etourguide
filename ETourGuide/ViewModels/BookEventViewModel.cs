﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class BookEventViewModel
    {
        public Event Event { get; set; }
        public int EventId { get; set; }
        public int Persons { get; set; }
        public int TotalBill { get; set; }
        public string PaymentMethod { get; set; }
    }
}