﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class AnswersViewModel
    {
        public int QuestionId { get; set; }
        public IEnumerable<Question> Questions { get; set; }
        public string Statement { get; set; }
    }
}