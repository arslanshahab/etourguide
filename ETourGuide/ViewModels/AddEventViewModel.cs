﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class AddEventViewModel
    {
        public Event Event{ get; set; }
        public IEnumerable<Place> Places { get; set; }
        public int PlacesId { get; set; }
        public IEnumerable<EventType> EventTypes { get; set; }
        public int EventTypesId { get; set; }
    }
}