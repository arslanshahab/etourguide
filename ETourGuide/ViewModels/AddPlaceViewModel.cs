﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class AddPlaceViewModel
    {
        public Place Place{ get; set; }
        public IEnumerable<Province> Provinces { get; set; }
        public int ProvinceId { get; set; }
        public List<Question> Questions { get; set; }
        public List<Answer> Answers { get; set; }
    }
}