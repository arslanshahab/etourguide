﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class BookingsHistoryViewModel
    {
        public List<TripBooking> TripBooking { get; set; }
        public List<TripBookingPlaces> TripBookingPlaces { get; set; }
        public List<Booking> Bookings { get; set; }
    }
}