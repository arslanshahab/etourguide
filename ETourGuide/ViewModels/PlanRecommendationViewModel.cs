﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class PlanRecommendationViewModel
    {
        public List<PlaceType> PlaceTypes { get; set; }
        public List<Province> Provinces { get; set; }
        public List<int> Days { get; set; }
        public string SelectedPlace { get; set; }
        public int SelectedDays { get; set; }
        public int SelectedProvince { get; set; }
    }
}