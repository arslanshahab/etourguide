﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class PaymentViewModel
    {
        public Event Event { get; set; }
        public int EventId { get; set; }
        public int Persons { get; set; }
        public int TotalBill { get; set; }
        public string PaymentMethod { get; set; }
        public string CardNumber { get; set; }
        public string AccountHolder { get; set; }
        public int CVC { get; set; }
        public string MobileNumber { get; set; }

    }
}