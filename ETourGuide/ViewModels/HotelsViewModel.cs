﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class HotelsViewModel
    {
        public Hotel Hotel { get; set; }
        public IEnumerable<Place> Places { get; set; }
        public int PlaceId { get; set; }
    }
}