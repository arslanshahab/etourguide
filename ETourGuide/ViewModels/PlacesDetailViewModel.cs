﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class PlacesDetailViewModel
    {
        public Place Place { get; set; }
        public IEnumerable<Hotel> Hotels { get; set; }
        public List<PlaceImage> PlaceImages { get; set; }
    }
}