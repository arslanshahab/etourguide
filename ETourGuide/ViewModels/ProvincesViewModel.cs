﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class ProvincesViewModel
    {
        public List<Province> Provinces{ get; set; }
        public List<Place> Places { get; set; }
    }
}