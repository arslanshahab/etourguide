﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class CartChangeHotelViewModel
    {
        public int CartPlaceId { get; set; }
        public List<Hotel> Hotels { get; set; }
    }
}