﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class SummaryViewModel
    {
        public Cart Cart { get; set; }
        public List<CartPlaces> CartPlaces { get; set; }
    }
}