﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class PaymentInfoViewModel
    {
        public Cart Cart { get; set; }
        public string StripeToken { get; set; }
    }
}