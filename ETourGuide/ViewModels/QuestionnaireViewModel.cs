﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class QuestionnaireViewModel
    {
        public List<Question> Questions { get; set; }
        public List<Answer> Answers { get; set; }
    }
}