﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class TripBookingsViewModel
    {
        public TripBooking TripBooking { get; set; }
        public List<TripBookingPlaces> TripBookingPlaces { get; set; }
    }
}