﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ETourGuide.ViewModels
{
    public class CollectDetailsViewModel
    {
        public Place Place{ get; set; }
        public Hotel Hotel { get; set; }
        public Transport Transport { get; set; }
        [DisplayName("No. of Persons")]
        public int Persons { get; set; }
        [DisplayName("Want Our Transport Services ?")]
        public bool IsTransportSelected { get; set; }
        [DisplayName("No. of Days")]
        public int Days { get; set; }
    }
}