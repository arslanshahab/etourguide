﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using Microsoft.AspNet.Identity;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    public class PaymentController : Controller
    {
        private ApplicationDbContext _context;

        public PaymentController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Payment
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Select(BookEventViewModel bookEvent)
        {
            var eventToBook = _context.Events.SingleOrDefault(x => x.Id == bookEvent.EventId);
            bookEvent.Event = eventToBook;

            var paymentModel = new PaymentViewModel
            {
                Event = eventToBook,
                EventId = eventToBook.Id,
                PaymentMethod = bookEvent.PaymentMethod,
                TotalBill = bookEvent.TotalBill,
                Persons = bookEvent.Persons
            };

            return View(paymentModel);
        }

        public ActionResult TripPayment()
        {
            var userId = User.Identity.GetUserId();
            var cart = _context.Carts.SingleOrDefault(x => x.UserId == userId);
            var paymentModel = new PaymentInfoViewModel{
                Cart = cart
            };
            return View(paymentModel);
        }


        public ActionResult PaymentCharge(PaymentInfoViewModel paymentInfo)
        {
            var userId = User.Identity.GetUserId();
            var userCart = _context.Carts.SingleOrDefault(x => x.UserId == userId);
            StripeConfiguration.ApiKey = "sk_test_vk5kgko57LUhoPxNZ5daQ704002zI4Pboz";

            var token = paymentInfo.StripeToken;

            var options = new ChargeCreateOptions
            {
                Amount = Convert.ToInt32(userCart.Bill * 0.25),
                Currency = "usd",
                Description = "Charge Created for trip booking",
                Source = token,
                Metadata = new Dictionary<String, String>() { { "UserEmail", User.Identity.GetUserName() } },
                StatementDescriptor = "Syahat.PK Trip Payment",

            };
            var service = new ChargeService();
            Charge charge = service.Create(options);

            return RedirectToAction("SaveTrip","TripBooking");
        }

    }
}