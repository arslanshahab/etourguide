﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    public class ProvincesController : Controller
    {
        private ApplicationDbContext _context;

        public ProvincesController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: /Provinces
        public ActionResult Index()
        {
            var provinces = _context.Provinces.ToList();
            var places = _context.Places.ToList();

            var provinceModel = new ProvincesViewModel
            {
                Places = places,
                Provinces = provinces
            };

            return View(provinceModel);
        }
    }
}