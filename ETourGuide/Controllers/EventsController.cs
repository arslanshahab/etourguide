﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    public class EventsController : Controller
    {
        private ApplicationDbContext _context;

        public EventsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Events
        public ActionResult Index()
        {
            var events = _context.Events.Where(e => e.Date > DateTime.Now).ToList();

            return View(events);
        }

        [Authorize(Roles ="Customer")]
        public ActionResult Book(int Id)
        {
            var eventToBook = _context.Events.SingleOrDefault(x => x.Id == Id);
            var bookingModel = new BookEventViewModel
            {
                Event = eventToBook,
                EventId = eventToBook.Id
            };

            return View(bookingModel);
        }

        public ActionResult Save(PaymentViewModel payment)
        {
            var userId = User.Identity.GetUserId();
            var bookings = new Booking
            {
                Advance = payment.TotalBill * 0.2,
                Bill = payment.TotalBill,
                EventId = payment.EventId,
                PaymentMethod = payment.PaymentMethod,
                BookingDate = DateTime.Now,
                Persons = payment.Persons,
                UserId = userId,
                Status = "Pending"
            };

            _context.Bookings.Add(bookings);
            _context.SaveChanges();

            return View();
        }

        public ActionResult Details(int Id)
        {
            var detailedEvent = _context.Events.Where(e => e.Id == Id).SingleOrDefault();

            return View(detailedEvent);
        }

        public ActionResult Cultures()
        {
            var events = _context.Events.Where(e => e.Date > DateTime.Now && e.EventTypeId == 1).ToList();

            return View(events);
        }

        public ActionResult Religious()
        {
            var events = _context.Events.Where(e => e.Date > DateTime.Now && e.EventTypeId == 2).ToList();

            return View(events);
        }


        public ActionResult Local()
        {
            var events = _context.Events.Where(e => e.Date > DateTime.Now && e.EventTypeId == 3).ToList();

            return View(events);
        }

    }
}