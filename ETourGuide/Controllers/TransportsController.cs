﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    public class TransportsController : Controller
    {
        private ApplicationDbContext _context;

        public TransportsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: /Transports
        public ActionResult Index()
        {
            var vehicles = _context.Transport.ToList();

            return View(vehicles);
        }

    }
}