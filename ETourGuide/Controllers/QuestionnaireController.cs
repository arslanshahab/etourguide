﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    public class QuestionnaireController : Controller
    {
        private ApplicationDbContext _context;

        public QuestionnaireController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Questionnaire
        public ActionResult Index()
        {
            var questions = _context.Questions.ToList();
            var answers = _context.Answers.ToList();

            var questionnaire = new QuestionnaireViewModel
            {
                Answers = answers,
                Questions = questions
            };
            return View(questionnaire);
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Save()
        {
            var formdata = Request.Form;
            var ans = formdata.Keys[1];

            return RedirectToAction("Index");
        }
    }
}