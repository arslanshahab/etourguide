﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    public class ExploreController : Controller
    {
        private ApplicationDbContext _context;

        public ExploreController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Explore
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Provinces()
        {
            return RedirectToAction("Index", "Provinces");
        }

        public ActionResult Events()
        {
            return View();
        }
        
    }
}