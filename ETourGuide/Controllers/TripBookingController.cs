﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using Microsoft.AspNet.Identity;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    [Authorize(Roles ="Customer")]
    public class TripBookingController : Controller
    {
        private ApplicationDbContext _context { get; set; }

        public TripBookingController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: TripBooking
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Book()
        {
            return View();
        }

        public ActionResult Summary()
        {
            var userId = User.Identity.GetUserId();

            var userCart = _context.Carts.Include("Transport").Where(x => x.UserId == userId).SingleOrDefault();
            var cartPlaces = _context.CartPlaces.Include("Place").Include("Hotel").Where(x => x.CartId == userCart.Id).ToList();

            var summaryViewModel = new SummaryViewModel
            {
                Cart = userCart,
                CartPlaces = cartPlaces
            };

            return View(summaryViewModel);
        }

        public ActionResult SaveTrip()
        {
            var userId = User.Identity.GetUserId();

            var userCart = _context.Carts.Where(x => x.UserId == userId).SingleOrDefault();
            var cartPlaces = _context.CartPlaces.Include("Place").Include("Hotel").Where(x => x.CartId == userCart.Id).ToList();

            var distanceTravelled = cartPlaces.Max(x => x.Place.Distance);

            var tripBooking = new TripBooking
            {
                Advance = Convert.ToInt32(userCart.Bill * 0.25),
                Bill = userCart.Bill,
                UserId = userId,
                Date = DateTime.Now,
                TransportId = 1,
                DistanceTravelled = distanceTravelled,
                Status = "Pending"
            };

            _context.TripBookings.Add(tripBooking);
            _context.SaveChanges();

            var bookingId = _context.TripBookings.Max(x => x.Id);

            foreach (var item in cartPlaces)
            {
                var bookindDetails = new TripBookingPlaces
                {
                    TripBookingId = bookingId,
                    PlaceId = item.PlaceId
                };

                _context.TripBookingPlaces.Add(bookindDetails);
                _context.CartPlaces.Remove(item);
            }

            _context.SaveChanges();

            SendNotification("Order has been placed successfully");

            var emailBody = new StringBuilder();

            emailBody.AppendLine("<h2 style='background:teal;color:#fff;text-align:center;'>Your Order has been placed</h2>");
            emailBody.AppendLine("<p>Your total bill is : <b>" + userCart.Bill + "</b></p>");
            emailBody.AppendLine("<p>Amount paid as advance : <b>" + Convert.ToInt32(userCart.Bill * 0.25) + "</b></p>");
            emailBody.AppendLine("<p>Amount due : <b>" + Convert.ToInt32(userCart.Bill * 0.75) + "</b></p>");
            emailBody.AppendLine("<p><i>Thank you for choosing Syahat.PK, we will make sre to make this trip rememberable</><p>");


            SendEmail(emailBody.ToString(), "Order Placed Successfully");

            return RedirectToAction("ThankYou");
        }

        private Task SendEmail(string body, string subject)
        {
            var userId = User.Identity.GetUserId();

            var email = _context.Users.Where(x => x.Id == userId).Select(x => x.Email).SingleOrDefault();

            // Create an Web transport for sending email.  
            var myMessage = new SendGridMessage();
            myMessage.AddTo(email);
            myMessage.From = new MailAddress("arslanshahab@gmail.com", "Syahat PK");
            myMessage.Subject = subject;
            myMessage.Html = body;
            var credentials = new NetworkCredential("arslanshahab", "Allah is 1");

            // Create a Web transport for sending email.
            var transportWeb = new Web(credentials);

            // Send the email.
            if (transportWeb != null)
            {
                return transportWeb.DeliverAsync(myMessage);
            }
            else
            {
                Trace.TraceError("Failed to create Web transport.");
                return Task.FromResult(0);
            }
        }

        public void SendNotification(string text)
        {
            var userId = User.Identity.GetUserId();

            var notification = _context.Notifications.SingleOrDefault(x => x.UserId == userId);
            if (notification == null)
            {
                var newNotification = new Notification
                {
                    UserId = userId
                };
                _context.Notifications.Add(newNotification);
                _context.SaveChanges();

                notification = _context.Notifications.SingleOrDefault(x => x.UserId == userId);
            }

            var userNotification = new UserNotification
            {
                NotificationId = notification.Id,
                Date = DateTime.Now,
                Text = text,
                Type = "customer"
            };

            _context.UserNotifications.Add(userNotification);
            _context.SaveChanges();
        }

        public ActionResult ThankYou()
        {
            return View();
        }

        public ActionResult History()
        {

            var userId = User.Identity.GetUserId();
            var tripItem = new BookingsHistoryViewModel
            {
                TripBooking = _context.TripBookings.Where(x=>x.UserId == userId).ToList(),
                Bookings = _context.Bookings.Include("Event").Where(x => x.UserId == userId).ToList()
            };


            return View(tripItem);
        }

        public ActionResult CancelTrip(int id)
        {
            var trip = _context.TripBookings.Where(x => x.Id == id).SingleOrDefault();
            trip.Status = "Cancelled";

            _context.SaveChanges();

            return RedirectToAction("History");
        }


        public ActionResult CancelEvent(int id)
        {
            var events = _context.Bookings.Where(x => x.Id == id).SingleOrDefault();
            events.Status = "Cancelled";

            _context.SaveChanges();

            return RedirectToAction("History");

        }
    }
}