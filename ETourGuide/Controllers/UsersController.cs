﻿using ETourGuide.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    [Authorize(Roles ="Customer")]
    public class UsersController : Controller
    {
        private ApplicationDbContext _context;

        public UsersController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Users
        public ActionResult Index()
        {
            var currentUserId = User.Identity.GetUserId();
            var profile = _context.UserProfiles.SingleOrDefault(p => p.UserId == currentUserId);

            return View(profile);
        }
        
    }
}