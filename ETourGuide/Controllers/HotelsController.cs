﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    public class HotelsController : Controller
    {
        private ApplicationDbContext _context;

        public HotelsController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Hotels
        public ActionResult Index()
        {
            var hotels = _context.Hotels.ToList();
            return View(hotels);
        }

        public ActionResult Details(int Id)
        {
            var hotel = _context.Hotels.SingleOrDefault(h => h.Id == Id);

            return View(hotel);
        }
    }
}