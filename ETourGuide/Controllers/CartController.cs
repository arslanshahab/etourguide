﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    public class CartController : Controller
    {
        private ApplicationDbContext _context;

        public CartController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Cart
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddRecommendedPlaces(RecommendationsViewModel selectedRecommendations)
        {
            var receivedPlaces = selectedRecommendations.Places.Where(x => x.IsSelected).ToList();
            var placesIds = receivedPlaces.Select(x => x.Id).ToList();
            var selectedPlaces = new List<Place>();

            foreach (var id in placesIds)
            {
                selectedPlaces.Add(_context.Places.Where(x => x.Id == id).SingleOrDefault());
            }

            var userId = User.Identity.GetUserId();

            if (selectedPlaces != null)
            {
                var userCartId = _context.Carts.Where(x => x.UserId == userId).Select(x => x.Id).SingleOrDefault();

                if (userCartId < 1)
                {
                    var cart = new Cart
                    {
                        UserId = userId
                    };

                    _context.Carts.Add(cart);
                    _context.SaveChanges();

                    userCartId = _context.Carts.Max(c => c.Id);
                }
                foreach (var item in selectedPlaces)
                {
                    var result = _context.CartPlaces.Where(x => x.CartId == userCartId).Select(x => x.PlaceId).ToList().Contains(item.Id);
                    if (!_context.CartPlaces.Where(x => x.CartId == userCartId).Select(x => x.PlaceId).Contains(item.Id))
                    {
                        var hotelId = _context.Hotels.Where(x => x.PlaceId == item.Id).OrderBy(x=>x.Rent).Select(x => x.Id).FirstOrDefault();
                        if (hotelId > 0)
                        {
                            var cartItem = new CartPlaces
                            {
                                CartId = userCartId,
                                PlaceId = item.Id,
                                HotelId = hotelId
                            };
                            _context.CartPlaces.Add(cartItem);
                        }
                        else
                        {
                            var cartItem = new CartPlaces
                            {
                                CartId = userCartId,
                                PlaceId = item.Id
                            };
                            _context.CartPlaces.Add(cartItem);
                        }

                    }

                }

                _context.SaveChanges();
            }

            return RedirectToAction("Summary", "TripBooking");
        }

        public ActionResult AddSinglePlace(Place place)
        {
            var selectedPlace = _context.Places.Where(x => x.Id == place.Id).SingleOrDefault();

            var userId = User.Identity.GetUserId();

            if (selectedPlace != null)
            {
                var userCartId = _context.Carts.Where(x => x.UserId == userId).Select(x => x.Id).SingleOrDefault();

                if (userCartId < 1)
                {
                    var cart = new Cart
                    {
                        UserId = userId
                    };

                    _context.Carts.Add(cart);
                    _context.SaveChanges();

                    userCartId = _context.Carts.Max(c => c.Id);
                }

                if (!_context.CartPlaces.Where(x => x.CartId == userCartId).Select(x => x.PlaceId).Contains(selectedPlace.Id))
                {
                    var hotelId = _context.Hotels.Where(x => x.PlaceId == selectedPlace.Id).OrderBy(x => x.Rent).Select(x => x.Id).FirstOrDefault();
                    if (hotelId > 0)
                    {
                        var cartItem = new CartPlaces
                        {
                            CartId = userCartId,
                            PlaceId = selectedPlace.Id,
                            HotelId = hotelId
                        };
                        _context.CartPlaces.Add(cartItem);
                    }
                    else
                    {
                        var cartItem = new CartPlaces
                        {
                            CartId = userCartId,
                            PlaceId = selectedPlace.Id
                        };
                        _context.CartPlaces.Add(cartItem);
                    }
                }

                _context.SaveChanges();
            }

            return RedirectToAction("Summary", "TripBooking");

        }

        public ActionResult Delete(int id)
        {
            var cartPlace = _context.CartPlaces.SingleOrDefault(x => x.Id == id);
            _context.CartPlaces.Remove(cartPlace);

            _context.SaveChanges();

            return RedirectToAction("Summary", "TripBooking");

        }

        public ActionResult ChangeCartHotel(int cartPlaceId)
        {
            var userId = User.Identity.GetUserId();

            var userCartId = _context.Carts.Where(x => x.UserId == userId).Select(x => x.Id).SingleOrDefault();

            var cartPlaces = _context.CartPlaces.Where(x => x.CartId == userCartId && x.Id == cartPlaceId).SingleOrDefault();

            var hotels = _context.Hotels.Include("Place").Where(x => x.PlaceId == cartPlaces.PlaceId).ToList();

            var changeHotelViewModel = new CartChangeHotelViewModel
            {
                CartPlaceId = cartPlaceId,
                Hotels = hotels
            };

            return View(changeHotelViewModel);
        }

        public ActionResult SaveHotel(int cartPlaceId, int hotelId)
        {
            var userId = User.Identity.GetUserId();

            var userCartId = _context.Carts.Where(x => x.UserId == userId).Select(x => x.Id).SingleOrDefault();

            var cartPlaces = _context.CartPlaces.Where(x => x.CartId == userCartId && x.Id == cartPlaceId).SingleOrDefault();

            cartPlaces.HotelId = hotelId;

            _context.SaveChanges();

            return RedirectToAction("Summary", "TripBooking");
        }


        public ActionResult AddTransportToCart()
        {
            var transports = _context.Transport.ToList();
            return View(transports);
        }

        public ActionResult SaveTransport(int transportId)
        {
            var userId = User.Identity.GetUserId();

            var userCart = _context.Carts.Where(x => x.UserId == userId).SingleOrDefault();

            userCart.TransportId = transportId;

            _context.SaveChanges();

            return RedirectToAction("Summary", "TripBooking");
        }

        public ActionResult CollectPayment(SummaryViewModel summaryViewModel)
        {
            var userId = User.Identity.GetUserId();
            var cart = _context.Carts.Where(x => x.UserId == userId).SingleOrDefault();

            cart.Bill = summaryViewModel.Cart.Bill;
            if (summaryViewModel.CartPlaces != null)
            {

                foreach (var item in summaryViewModel.CartPlaces)
                {
                    var cartPlace = _context.CartPlaces.Where(x => x.CartId == cart.Id && x.Id == item.Id).FirstOrDefault();
                    cartPlace.Days = item.Days;
                }

            _context.SaveChanges();
            }

            return RedirectToAction("TripPayment","Payment");
        }
    }

}