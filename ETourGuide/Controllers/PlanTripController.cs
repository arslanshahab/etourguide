﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    [Authorize(Roles = "Customer")]
    public class PlanTripController : Controller
    {
        private ApplicationDbContext _context;

        public PlanTripController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: PlanTrip
        public ActionResult Index()
        {
            var PlaceTypes = new List<PlaceType>();

            var mountain = new PlaceType
            {
                Id = 1,
                Name = "Mountain"
            };
            var lake = new PlaceType
            {
                Id = 2,
                Name = "Lake"
            };
            var desert = new PlaceType
            {
                Id = 3,
                Name = "Desert"
            };
            var historic = new PlaceType
            {
                Id = 4,
                Name = "Historical"
            };
            PlaceTypes.Add(mountain);
            PlaceTypes.Add(lake);
            PlaceTypes.Add(desert);
            PlaceTypes.Add(historic);


            var viewModel = new PlanRecommendationViewModel
            {
                PlaceTypes = PlaceTypes,
                Provinces = _context.Provinces.ToList()

            };

            return View(viewModel);
        }

        public ActionResult Recommendations(PlanRecommendationViewModel placeType)
        {
            var places = _context.Places.Where(x => x.Type == placeType.SelectedPlace && x.Days == placeType.SelectedDays && x.ProvinceId == placeType.SelectedProvince).ToList();

            var hotels = _context.Hotels.ToList();
            var recommendations = new RecommendationsViewModel
            {
                Places = places,
                Hotels = hotels
            };
            return View(recommendations);
        }

        public ActionResult Details(int Id)
        {
            var place = _context.Places.SingleOrDefault(p => p.Id == Id);

            return RedirectToAction("Details", "Places", new { Id, show = true });
        }



        public ActionResult SelectHotel(int Id)
        {
            var selectedPlace = _context.Places.SingleOrDefault(p => p.Id == Id);
            var hotels = _context.Hotels.Where(h => h.PlaceId == selectedPlace.Id).ToList();

            return View(hotels);
        }

        public ActionResult SelectTransport(int Id)
        {
            var transports = _context.Transport.ToList();
            ViewBag.HotelId = _context.Hotels.Where(h => h.Id == Id).Select(h => h.Id).SingleOrDefault();
            return View(transports);
        }

        public ActionResult CollectDetails(int Id, int hotelId)
        {
            var selectedHotel = _context.Hotels.SingleOrDefault(h => h.Id == hotelId);
            var selectedPlace = _context.Places.SingleOrDefault(p => p.Id == selectedHotel.PlaceId);
            var collectDetails = new CollectDetailsViewModel
            {
                Place = selectedPlace,
                Hotel = selectedHotel
            };

            return View(collectDetails);
        }

        public ActionResult PaymentInfo(int amount)
        {
            return View();
        }

        public ActionResult PaymentCharge(PaymentInfoViewModel paymentInfo)
        {

            StripeConfiguration.ApiKey = "sk_test_vk5kgko57LUhoPxNZ5daQ704002zI4Pboz";

            var token = paymentInfo.StripeToken;

            var options = new ChargeCreateOptions
            {
                Amount = 999,
                Currency = "usd",
                Description = "Example charge",
                Source = token,
                Metadata = new Dictionary<String, String>() { { "OrderId", "6735" } },
                StatementDescriptor = "Syahat.PK Trip Payment",

            };
            var service = new ChargeService();
            Charge charge = service.Create(options);

            return View();
        }

        public ActionResult TestPlaces()
        {
            var places = _context.Places.ToList();
            var viewModel = new TestPlaceViewModel
            {
                Places = places
            };
            return View(viewModel);
        }

        public ActionResult SaveTest(TestPlaceViewModel viewModel)
        {
            return Content("OK");
        }

        public ActionResult TestRecommend(RecommendationsViewModel viewModel)
        {
            return Content("OK");
        }
        [AllowAnonymous]
        public ActionResult TestQuest()
        {
            var PlaceTypes = new List<PlaceType>();

            var mountain = new PlaceType
            {
                Id = 1,
                Name = "Mountain"
            };
            var lake = new PlaceType
            {
                Id = 2,
                Name = "Lake"
            };
            var desert = new PlaceType
            {
                Id = 3,
                Name = "Desert"
            };
            var historic = new PlaceType
            {
                Id = 4,
                Name = "Historical"
            };
            PlaceTypes.Add(mountain);
            PlaceTypes.Add(lake);
            PlaceTypes.Add(desert);
            PlaceTypes.Add(historic);


            var viewModel = new PlanRecommendationViewModel
            {
                PlaceTypes = PlaceTypes,
                Provinces = _context.Provinces.ToList()

            };

            return View(viewModel);
        }

        public ActionResult SaveQuest(PlanRecommendationViewModel placeType)
        {
            return View(placeType);
        }

        public ActionResult SaveDays(PlanRecommendationViewModel placeType)
        {
            placeType.Provinces = _context.Provinces.ToList();
            return View(placeType);
        }

        public ActionResult SaveProvince(PlanRecommendationViewModel placeType)
        {
            var places = _context.Places.Where(x => x.Type == placeType.SelectedPlace && x.Days == placeType.SelectedDays && x.ProvinceId ==  placeType.SelectedProvince).ToList();
             return View(placeType);
        }

    }
}