﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    public class PlacesController : Controller
    {
        private ApplicationDbContext _context;

        public PlacesController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Places
        public ActionResult Index()
        {
            var places = _context.Places.ToList();

            return View(places);
        }

        public ActionResult Details(int Id, bool show = false)
        {
            var place = _context.Places.SingleOrDefault(p => p.Id == Id);
            var hotels = _context.Hotels.Where(h => h.PlaceId == place.Id).ToList();
            var placeImages = _context.PlaceImages.Where(x => x.PlaceId == place.Id).ToList();

            var placeDetail = new PlacesDetailViewModel
            {
                Place = place,
                Hotels = hotels,
                PlaceImages = placeImages
            };
            if (show)
            {
                ViewBag.isPlanView = true;
            }
            return View(placeDetail);
        }
    }
}