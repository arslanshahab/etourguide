﻿using ETourGuide.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Controllers
{
    public class NotificationController : Controller
    {
        private ApplicationDbContext _context { get; set; }

        public NotificationController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: Notification
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetNewNotifications()
        {
            // GET: Customer/NotificationCustomer
            var userId = User.Identity.GetUserId();

            var notificationId = _context.Notifications.Where(x => x.UserId == userId).Select(n => n.Id).SingleOrDefault();

            var notifications = _context.UserNotifications
                            .Where(x => x.NotificationId == notificationId && !x.IsRead && x.Type.ToLower() == "customer")
                            .Include(x => x.Notification)
                            .Select(un => un.Text)
                            .ToList();


            return Json(notifications, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetNewNotificationsAdmin()
        {
            // GET: Customer/NotificationCustomer
            var userId = User.Identity.GetUserId();

            var notificationId = _context.Notifications.Where(x => x.UserId == userId).Select(n => n.Id).SingleOrDefault();

            var notifications = _context.UserNotifications
                            .Where(x => x.NotificationId == notificationId && !x.IsRead && x.Type.ToLower() == "admin")
                            .Include(x => x.Notification)
                            .Select(un => un.Text)
                            .ToList();


            return Json(notifications, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public void MarkAsRead()
        {
            var userId = User.Identity.GetUserId();

            var notificationId = _context.Notifications.Where(x => x.UserId == userId).Select(n => n.Id).SingleOrDefault();

            var notifications = _context.UserNotifications
                            .Where(x => x.NotificationId == notificationId && !x.IsRead && x.Type.ToLower() == "customer")
                            .ToList();

            notifications.ForEach(n => n.IsRead = true);
            _context.SaveChanges();

        }
    }
}