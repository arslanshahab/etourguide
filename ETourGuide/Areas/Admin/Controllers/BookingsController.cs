﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Areas.Admin.Controllers
{
    public class BookingsController : Controller
    {
        private ApplicationDbContext _context;

        public BookingsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Admin/Bookings
        public ActionResult Index()
        {
            var bookings = _context.Bookings.Include("Event").Include("User").ToList();

            return View(bookings);
        }

        public ActionResult Event()
        {
            var bookings = _context.Bookings.Include("Event").Include("User").ToList();

            return View(bookings);
        }


        public ActionResult Trip()
        {
            var trips = new List<TripBookingsViewModel>();
            var bookings = _context.TripBookings.Include("User").ToList();

            foreach (var item in bookings)
            {
                var tripItem = new TripBookingsViewModel
                {
                    TripBooking = item,
                    TripBookingPlaces = _context.TripBookingPlaces.Include("Place").Where(x => x.TripBookingId == item.Id).ToList()
                };

                trips.Add(tripItem);
            }

            return View(trips);
        }
    }
}