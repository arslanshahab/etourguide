﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Areas.Admin.Controllers
{
    public class EventsController : Controller
    {
        private ApplicationDbContext _context;

        public EventsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Admin/Events
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add()
        {
            var places = _context.Places.ToList();
            var types = _context.EventTypes.ToList();

            var eventModel = new AddEventViewModel
            {
                Places = places,
                EventTypes = types
            };

            return View(eventModel);
        }

        public ActionResult Save(AddEventViewModel eventsAddView)
        {
            var httpRequest = HttpContext.Request;

            if (httpRequest.Files.Count > 0)
            {
                //getting files from the submit reques
                var postedFile = httpRequest.Files[0]; // only first file
                var filePath = HttpContext.Server.MapPath("~/Images/Events/" + postedFile.FileName); //converting absolute path to relative server path
                postedFile.SaveAs(filePath); //saving in local folder
                eventsAddView.Event.Image = postedFile.FileName; // setting image name for saving in database
            }

            eventsAddView.Event.PlaceId = eventsAddView.PlacesId;
            eventsAddView.Event.EventTypeId = eventsAddView.EventTypesId;

            _context.Events.Add(eventsAddView.Event);
            _context.SaveChanges();

            return RedirectToAction("Index",new { area=""});
        }
    }
}