﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin")]
    public class TransportsController : Controller
    {
        private ApplicationDbContext _context;

        public TransportsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Admin/Transports
        public ActionResult Index()
        {
            var vehicles = _context.Transport.ToList();

            return View(vehicles);
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Save(Transport transport)
        {
            var httpRequest = HttpContext.Request;

            if (httpRequest.Files.Count > 0)
            {
                //getting files from the submit reques
                var postedFile = httpRequest.Files[0]; // only first file
                var filePath = HttpContext.Server.MapPath("~/Images/Transport/" + postedFile.FileName); //converting absolute path to relative server path
                postedFile.SaveAs(filePath); //saving in local folder
                transport.Image = postedFile.FileName; // setting image name for saving in database
            }

            transport.IsAvailable = true;
            _context.Transport.Add(transport);
            _context.SaveChanges();

            return RedirectToAction("Index", new { controller = "Transports", area = "" });

        }

        public ActionResult Delete(int Id)
        {
            _context.Transport.Remove(_context.Transport.SingleOrDefault(x => x.Id == Id));
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}