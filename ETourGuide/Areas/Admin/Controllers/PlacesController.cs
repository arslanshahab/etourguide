﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class PlacesController : Controller
    {
        public int DistanceUnitPrice { get; set; }
        private ApplicationDbContext _context;

        public PlacesController()
        {
            _context = new ApplicationDbContext();
            DistanceUnitPrice = 10;
        }
        // GET: Admin/Places
        public ActionResult Index()
        {
            var places = _context.Places.Include("Province").ToList();

            return View(places);
        }

        public ActionResult Add()
        {
            var placeModel = new AddPlaceViewModel
            {
                Provinces = _context.Provinces.ToList(),
                Questions = _context.Questions.ToList(),
                Answers = _context.Answers.ToList()
            };
            return View(placeModel);
        }

        public ActionResult Edit(int id)
        {
            var place = _context.Places.Where(x => x.Id == id).SingleOrDefault();
            var placeModel = new AddPlaceViewModel
            {
                Provinces = _context.Provinces.ToList(),
                Questions = _context.Questions.ToList(),
                Answers = _context.Answers.ToList(),
                Place = place,
                ProvinceId = Convert.ToInt32(place.ProvinceId)
            };
            return View(placeModel);
        }

        [HttpPost]
        public ActionResult Save(AddPlaceViewModel addPlace)
        {

            var httpRequest = HttpContext.Request;
            if (addPlace.Place.Id == 0)
            {
                if (httpRequest.Files.Count > 0)
                {
                    //getting files from the submit reques
                    var postedFile = httpRequest.Files[0]; // only first file
                    var filePath = HttpContext.Server.MapPath("~/Images/Places/" + postedFile.FileName); //converting absolute path to relative server path
                    postedFile.SaveAs(filePath); //saving in local folder
                    addPlace.Place.Image = postedFile.FileName; // setting image name for saving in database
                }

                addPlace.Place.ProvinceId = addPlace.ProvinceId;

                addPlace.Place.PricePerDay = addPlace.Place.Distance * DistanceUnitPrice;


                _context.Places.Add(addPlace.Place);

                //adding model to context

                //saving context to database
                _context.SaveChanges();

                var placeId = _context.Places.Max(x => x.Id);

                if (httpRequest.Files.Count > 0)
                {

                    for (int i = 1; i < httpRequest.Files.Count; i++)
                    {
                        var postedFileGallery = httpRequest.Files[i];
                        var filePathGallery = HttpContext.Server.MapPath("~/Images/Places/" + postedFileGallery.FileName); //converting absolute path to relative server path
                        postedFileGallery.SaveAs(filePathGallery);

                        var descriptionImage = new PlaceImage
                        {
                            PlaceId = placeId,
                            Image = postedFileGallery.FileName
                        };

                        _context.PlaceImages.Add(descriptionImage);
                    }
                }
            }
            else
            {
                var currentPlace = _context.Places.SingleOrDefault(x => x.Id == addPlace.Place.Id);

                currentPlace.Name = addPlace.Place.Name;
                currentPlace.Distance = addPlace.Place.Distance;
                currentPlace.PricePerDay = addPlace.Place.Distance * DistanceUnitPrice;
                currentPlace.ProvinceId = addPlace.ProvinceId;
                currentPlace.Location = addPlace.Place.Location;
                currentPlace.Type = addPlace.Place.Type;
                currentPlace.Days = addPlace.Place.Days;

                if (httpRequest.Files.Count > 0)
                {

                    for (int i = 0; i < httpRequest.Files.Count; i++)
                    {
                        var postedFileGallery = httpRequest.Files[i];
                        var filePathGallery = HttpContext.Server.MapPath("~/Images/Places/" + postedFileGallery.FileName); //converting absolute path to relative server path
                        postedFileGallery.SaveAs(filePathGallery);

                        var descriptionImage = new PlaceImage
                        {
                            PlaceId = addPlace.Place.Id,
                            Image = postedFileGallery.FileName
                        };

                        _context.PlaceImages.Add(descriptionImage);
                    }
                }
            }


            _context.SaveChanges();
            return RedirectToAction("Index", new { controller = "Places", area = "Admin" });
        }

        public ActionResult Delete(int Id)
        {
            _context.Places.Remove(_context.Places.SingleOrDefault(x => x.Id == Id));
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}