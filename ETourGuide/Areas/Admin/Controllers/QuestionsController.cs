﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class QuestionsController : Controller
    {
        private ApplicationDbContext _context;

        public QuestionsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Admin/Questions
        public ActionResult Index()
        {
            var questions = _context.Questions.ToList();
            return View(questions);
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Save(Question question)
        {
            _context.Questions.Add(question);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}