﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class HotelsController : Controller
    {
        private ApplicationDbContext _context;

        public HotelsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Admin/Hotels
        public ActionResult Index()
        {
            var hotels = _context.Hotels.Include("Place").ToList();
            return View(hotels);
        }

        public ActionResult Add()
        {
            var places = _context.Places.ToList();
            var hotelsModel = new HotelsViewModel
            {
                Places = places
            };
            return View(hotelsModel);
        }

        [HttpPost]
        public ActionResult Save(HotelsViewModel hotelModel)
        {
            var httpRequest = HttpContext.Request;

            if (httpRequest.Files.Count > 0)
            {
                //getting files from the submit reques
                var postedFile = httpRequest.Files[0]; // only first file
                var filePath = HttpContext.Server.MapPath("~/Images/Hotels/" + postedFile.FileName); //converting absolute path to relative server path
                postedFile.SaveAs(filePath); //saving in local folder
                hotelModel.Hotel.Image = postedFile.FileName; // setting image name for saving in database
            }

            hotelModel.Hotel.IsOpened = true;
            hotelModel.Hotel.PlaceId = hotelModel.PlaceId;

            //adding model to context
            _context.Hotels.Add(hotelModel.Hotel);

            //saving context to database
            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var hotel = _context.Hotels.Where(x => x.Id == id).SingleOrDefault();
            _context.Hotels.Remove(hotel);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}