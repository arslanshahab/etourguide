﻿using ETourGuide.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Areas.Admin.Controllers
{
    public class ProvincesController : Controller
    {
        private ApplicationDbContext _context;

        public ProvincesController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Admin/Provinces
        public ActionResult Index()
        {
            var provinces = _context.Provinces.ToList();
            return View(provinces);
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Save(Province province)
        {
            _context.Provinces.Add(province);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}