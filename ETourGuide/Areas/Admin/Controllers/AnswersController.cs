﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin")]
    public class AnswersController : Controller
    {
        private ApplicationDbContext _context;

        public AnswersController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Admin/Answers
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add()
        {
            var questions = _context.Questions.ToList();

            var AnswersModel = new AnswersViewModel
            {
                Questions = questions
            };
            return View(AnswersModel);
        }

        public ActionResult Save(AnswersViewModel answers)
        {
            var answer = new Answer
            {
                QuestionId = answers.QuestionId,
                Statement = answers.Statement
            };

            _context.Answers.Add(answer);
            _context.SaveChanges();

            return RedirectToAction("Index","Questionnaire");
        }
    }
}