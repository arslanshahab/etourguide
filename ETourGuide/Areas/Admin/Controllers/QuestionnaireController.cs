﻿using ETourGuide.Models;
using ETourGuide.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ETourGuide.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class QuestionnaireController : Controller
    {
        private ApplicationDbContext _context;

        public QuestionnaireController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Admin/Questionnaire
        public ActionResult Index()
        {
            var questions = _context.Questions.ToList();
            var answers = _context.Answers.ToList();

            var questionnaire = new QuestionnaireViewModel
            {
                Answers = answers,
                Questions = questions
            };
            return View(questionnaire);
        }
        
    }
}