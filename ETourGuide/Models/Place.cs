﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class Place
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int? ProvinceId { get; set; }
        public Province Province { get; set; }
        public bool IsSelected { get; set; }
        public int PricePerDay { get; set; }
        public int Distance { get; set; }
        public string Type { get; set; }
        public int Days { get; set; }


    }
}