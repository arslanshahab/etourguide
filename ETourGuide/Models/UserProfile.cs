﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class UserProfile
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Image { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public ApplicationUser User { get; set; }

        public string UserId { get; set; }
    }
}