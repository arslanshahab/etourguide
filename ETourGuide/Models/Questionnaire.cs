﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class Questionnaire
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public ApplicationUser User { get; set; }
        public Question Question { get; set; }
        public Answer Answer { get; set; }
    }
}