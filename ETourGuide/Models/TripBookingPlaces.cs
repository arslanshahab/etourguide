﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class TripBookingPlaces
    {
        public int Id { get; set; }
        public int TripBookingId { get; set; }
        public TripBooking TripBooking { get; set; }
        public int PlaceId { get; set; }
        public Place Place { get; set; }
    }
}