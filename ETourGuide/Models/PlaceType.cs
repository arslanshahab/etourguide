﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class PlaceType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}