﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class Hotel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public int AvailableRooms { get; set; }
        public string Image { get; set; }
        public int Rent { get; set; }
        public bool IsOpened { get; set; }
        public int PlaceId { get; set; }
        public Place Place { get; set; }
    }
}