﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class Filter
    {
        public int Id { get; set; }
        public int FilterTextId { get; set; }
        public Question FilterText { get; set; }
    }
}