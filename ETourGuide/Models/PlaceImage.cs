﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class PlaceImage
    {
        public int Id { get; set; }
        public int PlaceId { get; set; }
        public Place Place { get; set; }
        public string Image { get; set; }
    }
}