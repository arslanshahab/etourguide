﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class RecommendationFilter
    {
        public int Id { get; set; }
        public int FilterId { get; set; }
        public int PlaceId { get; set; }
        public int FilterValueId { get; set; }
        public Filter Filter { get; set; }
        public Place Place { get; set; }
        public Answer FilterValue { get; set; }
    }
}