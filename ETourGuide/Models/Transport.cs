﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class Transport
    {
        public int Id { get; set; }

        [Display(Name ="Vehicle Name")]
        public string Name { get; set; }

        public string Model { get; set; }

        public string Type { get; set; }

        [Display(Name ="No. of Seats")]
        public int Capacity { get; set; }

        public string Image { get; set; }

        public bool IsAvailable { get; set; }
    }
}