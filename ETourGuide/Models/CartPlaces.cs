﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class CartPlaces
    {
        public int Id { get; set; }
        public int CartId { get; set; }
        public int PlaceId { get; set; }
        public int? HotelId { get; set; }
        public Cart Cart { get; set; }
        public Place Place { get; set; }
        public Hotel Hotel { get; set; }
        public int Days { get; set; }
    }
}