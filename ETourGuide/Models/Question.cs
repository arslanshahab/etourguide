﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class Question
    {
        public int Id { get; set; }
        public string Statement { get; set; }
    }
}