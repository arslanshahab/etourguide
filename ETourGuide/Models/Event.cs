﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class Event
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Days { get; set; }
        public DateTime Date { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public int PerHead { get; set; }
        public Place Place { get; set; }
        public int PlaceId { get; set; }
        public EventType EventType { get; set; }
        public int EventTypeId { get; set; }
    }
}