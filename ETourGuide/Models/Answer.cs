﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class Answer
    {
        public int Id { get; set; }
        public string Statement { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
    }
}