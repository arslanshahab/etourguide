﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class UserNotification
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public bool IsRead { get; set; }
        public int NotificationId { get; set; }
        public Notification Notification { get; set; }
    }
}