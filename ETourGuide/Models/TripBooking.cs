﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class TripBooking
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public DateTime Date { get; set; }
        public string Status { get; set; }
        public int Bill { get; set; }
        public bool IsPayment { get; set; }
        public int Advance { get; set; }
        public int TransportId { get; set; }
        public Transport Transport { get; set; }
        public int DistanceTravelled { get; set; }
    }
}