﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETourGuide.Models
{
    public class Booking
    {
        public int Id { get; set; }
        public double Advance { get; set; }
        public int Bill { get; set; }
        public Event Event { get; set; }
        public int EventId { get; set; }
        public int Persons { get; set; }
        public string PaymentMethod { get; set; }
        public DateTime BookingDate { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
        public string Status { get; set; }
    }
}