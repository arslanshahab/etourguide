namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedForeign : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transports", "HotelId", c => c.Int(nullable: false));
            CreateIndex("dbo.Transports", "HotelId");
            AddForeignKey("dbo.Transports", "HotelId", "dbo.Hotels", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transports", "HotelId", "dbo.Hotels");
            DropIndex("dbo.Transports", new[] { "HotelId" });
            DropColumn("dbo.Transports", "HotelId");
        }
    }
}
