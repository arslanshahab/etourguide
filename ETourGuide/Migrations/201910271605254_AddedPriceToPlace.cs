namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPriceToPlace : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Places", "PricePerDay", c => c.Int(nullable: false));
            DropColumn("dbo.CartPlaces", "PricePerDay");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CartPlaces", "PricePerDay", c => c.Int(nullable: false));
            DropColumn("dbo.Places", "PricePerDay");
        }
    }
}
