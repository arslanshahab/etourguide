namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedCarttransport : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Carts", "TransportId", "dbo.Transports");
            DropIndex("dbo.Carts", new[] { "TransportId" });
            AlterColumn("dbo.Carts", "TransportId", c => c.Int());
            CreateIndex("dbo.Carts", "TransportId");
            AddForeignKey("dbo.Carts", "TransportId", "dbo.Transports", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Carts", "TransportId", "dbo.Transports");
            DropIndex("dbo.Carts", new[] { "TransportId" });
            AlterColumn("dbo.Carts", "TransportId", c => c.Int(nullable: false));
            CreateIndex("dbo.Carts", "TransportId");
            AddForeignKey("dbo.Carts", "TransportId", "dbo.Transports", "Id", cascadeDelete: true);
        }
    }
}
