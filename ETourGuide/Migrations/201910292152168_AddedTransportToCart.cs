namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTransportToCart : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Carts", "TransportId", c => c.Int(nullable: false));
            CreateIndex("dbo.Carts", "TransportId");
            AddForeignKey("dbo.Carts", "TransportId", "dbo.Transports", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Carts", "TransportId", "dbo.Transports");
            DropIndex("dbo.Carts", new[] { "TransportId" });
            DropColumn("dbo.Carts", "TransportId");
        }
    }
}
