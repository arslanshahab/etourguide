namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPerHeadToEvent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "PerHead", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "PerHead");
        }
    }
}
