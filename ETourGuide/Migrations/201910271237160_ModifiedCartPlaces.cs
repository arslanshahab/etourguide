namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedCartPlaces : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CartPlaces", "HotelId", "dbo.Hotels");
            DropIndex("dbo.CartPlaces", new[] { "HotelId" });
            AlterColumn("dbo.CartPlaces", "HotelId", c => c.Int());
            CreateIndex("dbo.CartPlaces", "HotelId");
            AddForeignKey("dbo.CartPlaces", "HotelId", "dbo.Hotels", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CartPlaces", "HotelId", "dbo.Hotels");
            DropIndex("dbo.CartPlaces", new[] { "HotelId" });
            AlterColumn("dbo.CartPlaces", "HotelId", c => c.Int(nullable: false));
            CreateIndex("dbo.CartPlaces", "HotelId");
            AddForeignKey("dbo.CartPlaces", "HotelId", "dbo.Hotels", "Id", cascadeDelete: true);
        }
    }
}
