namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIsCheckedPropertyAnswer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "IsChecked", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Answers", "IsChecked");
        }
    }
}
