namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedImageToTransport : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transports", "Image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transports", "Image");
        }
    }
}
