namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedPlaceModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Places", "Type", c => c.String());
            AddColumn("dbo.Places", "Days", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Places", "Days");
            DropColumn("dbo.Places", "Type");
        }
    }
}
