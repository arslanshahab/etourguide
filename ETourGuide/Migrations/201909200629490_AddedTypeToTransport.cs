namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTypeToTransport : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transports", "Type", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transports", "Type");
        }
    }
}
