namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedQuestionnaire : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Answers", "IsSelected");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Answers", "IsSelected", c => c.Boolean(nullable: false));
        }
    }
}
