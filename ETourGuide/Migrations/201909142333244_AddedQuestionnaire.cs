namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedQuestionnaire : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Questionnaires",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        QuestionId = c.Int(nullable: false),
                        AnswerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Answers", t => t.AnswerId, cascadeDelete: false)
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.QuestionId)
                .Index(t => t.AnswerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Questionnaires", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Questionnaires", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.Questionnaires", "AnswerId", "dbo.Answers");
            DropIndex("dbo.Questionnaires", new[] { "AnswerId" });
            DropIndex("dbo.Questionnaires", new[] { "QuestionId" });
            DropIndex("dbo.Questionnaires", new[] { "UserId" });
            DropTable("dbo.Questionnaires");
        }
    }
}
