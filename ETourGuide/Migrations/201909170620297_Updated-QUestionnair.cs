namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedQUestionnair : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Questions", "SelectedAnswerId", c => c.Int(nullable: false));
            DropColumn("dbo.Answers", "IsChecked");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Answers", "IsChecked", c => c.Boolean(nullable: false));
            DropColumn("dbo.Questions", "SelectedAnswerId");
        }
    }
}
