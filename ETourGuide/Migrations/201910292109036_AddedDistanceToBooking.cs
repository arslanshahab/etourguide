namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDistanceToBooking : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TripBookings", "DistanceTravelled", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TripBookings", "DistanceTravelled");
        }
    }
}
