namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedStatusToTrip : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TripBookings", "Status", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TripBookings", "Status", c => c.Boolean(nullable: false));
        }
    }
}
