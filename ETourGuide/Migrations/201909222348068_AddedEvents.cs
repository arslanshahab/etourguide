namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedEvents : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Days = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Image = c.String(),
                        Description = c.String(),
                        PlaceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Places", t => t.PlaceId, cascadeDelete: true)
                .Index(t => t.PlaceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Events", "PlaceId", "dbo.Places");
            DropIndex("dbo.Events", new[] { "PlaceId" });
            DropTable("dbo.Events");
        }
    }
}
