namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOrdersTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TripBookingPlaces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TripBookingId = c.Int(nullable: false),
                        PlaceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Places", t => t.PlaceId, cascadeDelete: true)
                .ForeignKey("dbo.TripBookings", t => t.TripBookingId, cascadeDelete: true)
                .Index(t => t.TripBookingId)
                .Index(t => t.PlaceId);
            
            CreateTable(
                "dbo.TripBookings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        Bill = c.Int(nullable: false),
                        IsPayment = c.Boolean(nullable: false),
                        Advance = c.Int(nullable: false),
                        TransportId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Transports", t => t.TransportId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.TransportId);
            
            AddColumn("dbo.CartPlaces", "HotelId", c => c.Int(nullable: false));
            CreateIndex("dbo.CartPlaces", "HotelId");
            AddForeignKey("dbo.CartPlaces", "HotelId", "dbo.Hotels", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TripBookingPlaces", "TripBookingId", "dbo.TripBookings");
            DropForeignKey("dbo.TripBookings", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TripBookings", "TransportId", "dbo.Transports");
            DropForeignKey("dbo.TripBookingPlaces", "PlaceId", "dbo.Places");
            DropForeignKey("dbo.CartPlaces", "HotelId", "dbo.Hotels");
            DropIndex("dbo.TripBookings", new[] { "TransportId" });
            DropIndex("dbo.TripBookings", new[] { "UserId" });
            DropIndex("dbo.TripBookingPlaces", new[] { "PlaceId" });
            DropIndex("dbo.TripBookingPlaces", new[] { "TripBookingId" });
            DropIndex("dbo.CartPlaces", new[] { "HotelId" });
            DropColumn("dbo.CartPlaces", "HotelId");
            DropTable("dbo.TripBookings");
            DropTable("dbo.TripBookingPlaces");
        }
    }
}
