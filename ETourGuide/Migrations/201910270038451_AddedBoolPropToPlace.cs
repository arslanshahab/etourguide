namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBoolPropToPlace : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Places", "IsSelected", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Places", "IsSelected");
        }
    }
}
