namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlaceMultileImages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlaceImages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlaceId = c.Int(nullable: false),
                        Image = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Places", t => t.PlaceId, cascadeDelete: true)
                .Index(t => t.PlaceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlaceImages", "PlaceId", "dbo.Places");
            DropIndex("dbo.PlaceImages", new[] { "PlaceId" });
            DropTable("dbo.PlaceImages");
        }
    }
}
