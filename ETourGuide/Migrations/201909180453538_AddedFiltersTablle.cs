namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFiltersTablle : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Filters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RecommendationFilters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FilterId = c.Int(nullable: false),
                        PlaceId = c.Int(nullable: false),
                        HotelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Filters", t => t.FilterId, cascadeDelete: true)
                .ForeignKey("dbo.Hotels", t => t.HotelId, cascadeDelete: true)
                .ForeignKey("dbo.Places", t => t.PlaceId, cascadeDelete: true)
                .Index(t => t.FilterId)
                .Index(t => t.PlaceId)
                .Index(t => t.HotelId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RecommendationFilters", "PlaceId", "dbo.Places");
            DropForeignKey("dbo.RecommendationFilters", "HotelId", "dbo.Hotels");
            DropForeignKey("dbo.RecommendationFilters", "FilterId", "dbo.Filters");
            DropIndex("dbo.RecommendationFilters", new[] { "HotelId" });
            DropIndex("dbo.RecommendationFilters", new[] { "PlaceId" });
            DropIndex("dbo.RecommendationFilters", new[] { "FilterId" });
            DropTable("dbo.RecommendationFilters");
            DropTable("dbo.Filters");
        }
    }
}
