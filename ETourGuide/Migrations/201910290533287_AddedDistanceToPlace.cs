namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDistanceToPlace : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Places", "Distance", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Places", "Distance");
        }
    }
}
