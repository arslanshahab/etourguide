namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedHotelModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Hotels", "PlaceId", c => c.Int(nullable: false));
            CreateIndex("dbo.Hotels", "PlaceId");
            AddForeignKey("dbo.Hotels", "PlaceId", "dbo.Places", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Hotels", "PlaceId", "dbo.Places");
            DropIndex("dbo.Hotels", new[] { "PlaceId" });
            DropColumn("dbo.Hotels", "PlaceId");
        }
    }
}
