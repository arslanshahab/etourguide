namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventTypesModified : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Events", "EventType_Id", "dbo.EventTypes");
            DropIndex("dbo.Events", new[] { "EventType_Id" });
            RenameColumn(table: "dbo.Events", name: "EventType_Id", newName: "EventTypeId");
            AlterColumn("dbo.Events", "EventTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Events", "EventTypeId");
            AddForeignKey("dbo.Events", "EventTypeId", "dbo.EventTypes", "Id", cascadeDelete: true);
            DropColumn("dbo.Events", "EventId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Events", "EventId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Events", "EventTypeId", "dbo.EventTypes");
            DropIndex("dbo.Events", new[] { "EventTypeId" });
            AlterColumn("dbo.Events", "EventTypeId", c => c.Int());
            RenameColumn(table: "dbo.Events", name: "EventTypeId", newName: "EventType_Id");
            CreateIndex("dbo.Events", "EventType_Id");
            AddForeignKey("dbo.Events", "EventType_Id", "dbo.EventTypes", "Id");
        }
    }
}
