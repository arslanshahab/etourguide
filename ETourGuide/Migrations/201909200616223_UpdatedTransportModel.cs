namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedTransportModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Transports", "HotelId", "dbo.Hotels");
            DropIndex("dbo.Transports", new[] { "HotelId" });
            AddColumn("dbo.Transports", "Model", c => c.String());
            AddColumn("dbo.Transports", "Capacity", c => c.Int(nullable: false));
            DropColumn("dbo.Transports", "HotelId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Transports", "HotelId", c => c.Int(nullable: false));
            DropColumn("dbo.Transports", "Capacity");
            DropColumn("dbo.Transports", "Model");
            CreateIndex("dbo.Transports", "HotelId");
            AddForeignKey("dbo.Transports", "HotelId", "dbo.Hotels", "Id", cascadeDelete: true);
        }
    }
}
