namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifiedQuestionnaire : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "IsSelected", c => c.Boolean(nullable: false));
            DropColumn("dbo.Questions", "IsSelected");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Questions", "IsSelected", c => c.Boolean(nullable: false));
            DropColumn("dbo.Answers", "IsSelected");
        }
    }
}
