namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventTypes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EventTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Events", "EventId", c => c.Int(nullable: false));
            AddColumn("dbo.Events", "EventType_Id", c => c.Int());
            CreateIndex("dbo.Events", "EventType_Id");
            AddForeignKey("dbo.Events", "EventType_Id", "dbo.EventTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Events", "EventType_Id", "dbo.EventTypes");
            DropIndex("dbo.Events", new[] { "EventType_Id" });
            DropColumn("dbo.Events", "EventType_Id");
            DropColumn("dbo.Events", "EventId");
            DropTable("dbo.EventTypes");
        }
    }
}
