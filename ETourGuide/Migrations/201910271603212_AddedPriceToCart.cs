namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPriceToCart : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CartPlaces", "Days", c => c.Int(nullable: false));
            AddColumn("dbo.CartPlaces", "PricePerDay", c => c.Int(nullable: false));
            AddColumn("dbo.Carts", "Bill", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Carts", "Bill");
            DropColumn("dbo.CartPlaces", "PricePerDay");
            DropColumn("dbo.CartPlaces", "Days");
        }
    }
}
