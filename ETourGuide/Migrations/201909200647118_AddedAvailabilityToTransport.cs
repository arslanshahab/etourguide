namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAvailabilityToTransport : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transports", "IsAvailable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transports", "IsAvailable");
        }
    }
}
