namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LinkedPlaceToProvince : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Places", "ProvinceId", c => c.Int());
            CreateIndex("dbo.Places", "ProvinceId");
            AddForeignKey("dbo.Places", "ProvinceId", "dbo.Provinces", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Places", "ProvinceId", "dbo.Provinces");
            DropIndex("dbo.Places", new[] { "ProvinceId" });
            DropColumn("dbo.Places", "ProvinceId");
        }
    }
}
