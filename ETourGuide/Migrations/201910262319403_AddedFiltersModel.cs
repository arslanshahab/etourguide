namespace ETourGuide.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFiltersModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RecommendationFilters", "HotelId", "dbo.Hotels");
            DropIndex("dbo.RecommendationFilters", new[] { "HotelId" });
            AddColumn("dbo.Filters", "FilterTextId", c => c.Int(nullable: false));
            AddColumn("dbo.RecommendationFilters", "FilterValueId", c => c.Int(nullable: false));
            CreateIndex("dbo.Filters", "FilterTextId");
            CreateIndex("dbo.RecommendationFilters", "FilterValueId");
            AddForeignKey("dbo.Filters", "FilterTextId", "dbo.Questions", "Id", cascadeDelete: false);
            AddForeignKey("dbo.RecommendationFilters", "FilterValueId", "dbo.Answers", "Id", cascadeDelete: false);
            DropColumn("dbo.Filters", "Name");
            DropColumn("dbo.Filters", "Type");
            DropColumn("dbo.RecommendationFilters", "HotelId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RecommendationFilters", "HotelId", c => c.Int(nullable: false));
            AddColumn("dbo.Filters", "Type", c => c.String());
            AddColumn("dbo.Filters", "Name", c => c.String());
            DropForeignKey("dbo.RecommendationFilters", "FilterValueId", "dbo.Answers");
            DropForeignKey("dbo.Filters", "FilterTextId", "dbo.Questions");
            DropIndex("dbo.RecommendationFilters", new[] { "FilterValueId" });
            DropIndex("dbo.Filters", new[] { "FilterTextId" });
            DropColumn("dbo.RecommendationFilters", "FilterValueId");
            DropColumn("dbo.Filters", "FilterTextId");
            CreateIndex("dbo.RecommendationFilters", "HotelId");
            AddForeignKey("dbo.RecommendationFilters", "HotelId", "dbo.Hotels", "Id", cascadeDelete: true);
        }
    }
}
